#include "DirectoryCache.h"
#include <iostream>

// Ucitava cluster direktorijuma sa diska
bool FSopos::DirectoryCache::getClusterDisk () {
	part->readCluster (1, (char *)entries);
	return true;
}

// Smjesta cluster direktorijuma na disk
void FSopos::DirectoryCache::saveClusterDisk () {

	part->writeCluster (1, (char *)entries);

}
