#pragma once
#include "DirectoryCache.h"
#include <string>

class FileHandler;
class KernelFS;

namespace FSopos {

	class FileCache;

	class EntryCache {

		EntryCache () {};
		bool loadEntryNumber (int num); //ucitava entri sa diska
		bool loadEntryName (std::string name); // ucitava entri po imenu sa diska
		void storeOnDisk (); // smjesti entri na disk
		void createEntry (std::string name); // kreiraj entri
		void removeEntry (); // brise entri sa diska
		Entry& getEntry (); // vraca referencu na entri

		int	physicalOffset; // fizicki ofset u direkttorijumu
		DirectoryCache myDirCluster;

		friend class KernelFS;
		friend class KernelFile;
		friend class FileCache;

	};

}
