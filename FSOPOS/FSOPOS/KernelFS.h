#pragma once
#include "FileCache.h"
#include <unordered_map>
#include <Windows.h>

class KernelFS {

	enum MountedFlag {
		YES, NO //mountovana ili ne
	};

	friend class FS;
	friend class KernelFile;

	KernelFS ();
	~KernelFS ();
	char mount (Partition* partition);
	char unmount (char part);
	char format (char part);
	char readRootDir (char part, EntryNum n, Directory &d);
	char doesExist (char* fname);
	File* open (char* fname, char mode);
	char deleteFile (char* fname);
	std::string changeName (char* fname);

	Partition* symbols[26];
	MountedFlag partitionFlag[26];
	FSopos::BitVector* bitVctr[26];
	std::unordered_map<std::string, SRWLOCK> openedFiles[26];
	unsigned numberOfOpened[26] = { 0 };
	CRITICAL_SECTION partitionInUse[26];
};
