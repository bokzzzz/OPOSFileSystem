#pragma once
#include "BitVector.h"
#include "fs.h"

namespace FSopos {

	class EntryCache;

	class DirectoryCache {

		DirectoryCache () {};
		bool getClusterDisk (); // ucitava klaster sa diska
		void saveClusterDisk ();		// snima klaster na disk

		Entry entries[102] = {};
		char reserved[8] = {};
		BitVector* bitVctr;
		Partition* part;

		friend class EntryCache;
		friend class KernelFS;

	};

}
