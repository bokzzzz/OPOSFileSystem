#include "BitVector.h"
#include <cmath>
#include <stdexcept>
#include <Windows.h>

CRITICAL_SECTION BVCSection;

FSopos::BitVector::BitVector (Partition* partition) :myPartition (partition) {
	EnterCriticalSection (&BVCSection);
	myPartition->readCluster (0, (char*)cache);
	LeaveCriticalSection (&BVCSection);
}

FSopos::BitVector::~BitVector () {
	EnterCriticalSection (&BVCSection);
	update ();
	LeaveCriticalSection (&BVCSection);
}

ClusterNo  FSopos::BitVector::takeCluster () {
	EnterCriticalSection (&BVCSection);
	for (unsigned i = 0; i < ClusterSize; i++) {
		if (cache[i] != 0xFF) {//bajt 1111 1111
			for (int j = 0;; j++) {
				if ((cache[i] >> j) % 2 == 0) {//provjera koji je slobodan
					cache[i] ^= (1 << j);//setuje bit na 1
					update();
					LeaveCriticalSection (&BVCSection);
					return i * 8 + j;//vraca broj bajta sa kojeg je uzeo i ofset na njemu
				}
			}
		}
	}
	throw std::overflow_error ("Disk is full.");
}


void FSopos::BitVector::free (ClusterNo cluster) {
	EnterCriticalSection (&BVCSection);
	unsigned int	i = cluster / 8, j = cluster % 8;
	cache[i] ^= (1 << j);//xor sa 1 za ofset
	update();
	LeaveCriticalSection (&BVCSection);
}

void FSopos::BitVector::update () {

	EnterCriticalSection (&BVCSection);
	myPartition->writeCluster (0, (char*)cache);//iz kesa na disk
	LeaveCriticalSection (&BVCSection);
}

void FSopos::BitVector::initialize () {
	EnterCriticalSection (&BVCSection);
	for (unsigned i = 0; i < ClusterSize; i++) { cache[i] = 0; }// na disku sve na nulu
	takeCluster ();
	LeaveCriticalSection (&BVCSection);
}
