#include "KernelFS.h"
#include "file.h"
#include "KernelFile.h"
#include "FileCache.h"
#include "EntryCache.h"
#include "BitVector.h"

CRITICAL_SECTION crtcSec;

KernelFS::KernelFS () {

	for (Partition* &x : symbols) { x = nullptr; }
}

KernelFS::~KernelFS () {}

char KernelFS::mount (Partition * partition) {
	for (int i = 0; i < 26; i++) {
		if (symbols[i] == nullptr) {
			symbols[i] = partition;
			bitVctr[i] = new FSopos::BitVector(partition);
			InitializeCriticalSection(partitionInUse + i);
			partitionFlag[i] = NO;
			return 'A' + i;
		}
	}
	return '0';
}

char KernelFS::unmount (char part) {
	int partNumber = part - 'A';
	if (symbols[partNumber] == nullptr) { return 0; }
	partitionFlag[partNumber] = YES;
	EnterCriticalSection (partitionInUse + partNumber);
	bitVctr[partNumber]->update ();
	delete bitVctr[partNumber];
	symbols[partNumber] = nullptr;
	DeleteCriticalSection (partitionInUse + partNumber);
	return 1;
}

char KernelFS::format (char part) {
	int partNumber = part - 'A';
	if (symbols[partNumber] == 0) { return 0; }
	partitionFlag[partNumber] = YES;
	EnterCriticalSection (partitionInUse + partNumber);
	bitVctr[partNumber]->initialize ();
	bitVctr[partNumber]->takeCluster ();
	bitVctr[partNumber]->update ();
	char zeroBuffer[ClusterSize] = {};
	symbols[partNumber]->writeCluster (1, zeroBuffer);
	partitionFlag[partNumber] = NO;
	LeaveCriticalSection (partitionInUse + partNumber);
	return 1;
}

char KernelFS::readRootDir (char part, EntryNum n, Directory & d) {
	int partNumber = part - 'A';
	int i;
	FSopos::EntryCache entryCache;
	entryCache.myDirCluster.bitVctr = bitVctr[partNumber];
	entryCache.myDirCluster.part = symbols[partNumber];
	for(i=0;i<65 && entryCache.loadEntryNumber(n+i);i++){
		if (i == 64) { return 65; }
		d[i] = entryCache.getEntry ();
	}
	return i;
}

char KernelFS::doesExist (char * fname) {
	std::string name = changeName (fname);
	FSopos::EntryCache entryCache;
	entryCache.myDirCluster.bitVctr = bitVctr[*fname - 'A'];
	entryCache.myDirCluster.part = symbols[*fname - 'A'];
	return entryCache.loadEntryName (name);
}

File * KernelFS::open(char * fname, char mode) {
	int partNumber = *fname - 'A';
	if (partitionFlag[partNumber] == YES) { return nullptr; }
	File* filePtr = new File;
	filePtr->myImpl->myFS = this;
	filePtr->myImpl->absolutePath = fname;
	filePtr->myImpl->fileCache.bitVctr = bitVctr[partNumber];
	filePtr->myImpl->fileCache.myEntryCache.myDirCluster.bitVctr = bitVctr[partNumber];
	filePtr->myImpl->fileCache.myEntryCache.myDirCluster.part = symbols[partNumber];
	filePtr->myImpl->fileCache.part = symbols[partNumber];
	switch (mode)
	{
	case 'r':
		filePtr->myImpl->mode = 'r';
		if (filePtr->myImpl->fileCache.myEntryCache.loadEntryName (changeName (fname)) == false) {
			filePtr->myImpl->mode = '0';
			delete filePtr;
			filePtr = nullptr;
		} else {
			filePtr->myImpl->seek (0);
		}
		break;
	case 'w':
		filePtr->myImpl->mode = 'w';
		if (filePtr->myImpl->fileCache.myEntryCache.loadEntryName (changeName (fname)) == true) {
			filePtr->myImpl->seek (0);
			filePtr->myImpl->truncate ();
			filePtr->myImpl->fileCache.addClusterDisk ();//Dodaje prazan klaster
		} else {
			filePtr->myImpl->fileCache.myEntryCache.createEntry (changeName (fname));
			filePtr->myImpl->fileCache.addClusterDisk ();//doda klaster
			filePtr->myImpl->seek (0);//pozicioniranje na 0
		}
		break;
	case 'a':
		filePtr->myImpl->mode = 'a';
		if (filePtr->myImpl->fileCache.myEntryCache.loadEntryName (changeName (fname)) == true) {
			filePtr->myImpl->seek (filePtr->getFileSize ());
		} else {
			filePtr->myImpl->mode = '0';
			delete filePtr;
			filePtr = nullptr;
		}
		break;
	}
	EnterCriticalSection(&crtcSec);
	if (filePtr != nullptr) {
		if (numberOfOpened == 0) { EnterCriticalSection (partitionInUse + partNumber);//zabrana formatiranja particije i unmonta
		}
		numberOfOpened[partNumber]++;
	}
	LeaveCriticalSection(&crtcSec);
	return filePtr;
}

char KernelFS::deleteFile (char * fname) {
	int partNo = *fname - 'A';
	if (openedFiles[partNo].find (fname) != openedFiles[partNo].end ()) {
		if (TryAcquireSRWLockExclusive (&openedFiles[partNo][fname]) == 0) { return 0; }
	}
	std::string changeFName = changeName (fname);
	FSopos::FileCache fileCache;
	fileCache.myEntryCache.myDirCluster.bitVctr = bitVctr[partNo];
	fileCache.bitVctr = bitVctr[partNo];
	fileCache.myEntryCache.myDirCluster.part = symbols[partNo];
	fileCache.part = symbols[partNo];
	fileCache.myEntryCache.loadEntryName (changeFName);
	int i = 0;
	while (fileCache.freeClusterDisk(i++));
	bitVctr[partNo]->free (fileCache.myEntryCache.getEntry ().indexCluster);
	fileCache.myEntryCache.removeEntry ();
	return 1;
}

std::string KernelFS::changeName (char * fname) {
	std::string name = "           ";
	for (int i = 3, j = 0; j < 11 && fname[i] != 0; i++) {
		if (fname[i] != '.') {
			name[j] = fname[i];
			j++;
		} else { j = 8; }
	}
	return name;
}
