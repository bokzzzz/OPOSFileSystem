#include "KernelFile.h"
#include "KernelFS.h"

extern CRITICAL_SECTION crtcSec;

KernelFile::~KernelFile () {
	if (mode == '0'){ return; }
	int partNo = absolutePath[0] - 'A';
	auto& srwlock = myFS->openedFiles[partNo][absolutePath];
	EnterCriticalSection(&crtcSec);
	myFS->numberOfOpened[partNo]--;
	LeaveCriticalSection(&crtcSec);
	switch (mode) {
	case 'r':
		ReleaseSRWLockShared (&srwlock);
		break;
	case 'w': case 'a':
		ReleaseSRWLockExclusive (&srwlock);
		break;
	}
	EnterCriticalSection(&crtcSec);
	if (myFS->numberOfOpened[partNo] == 0) { LeaveCriticalSection (myFS->partitionInUse + partNo); }
	LeaveCriticalSection(&crtcSec);
}

char KernelFile::write (BytesCnt size, char * buffer) {
	if (mode == 'r') { return 0; }
	int Counter = 0;//broj upisanih bajtova
	if (fileCache.myEntryCache.getEntry ().size < position + size) {//prosiruje se fajl ako ima dodatni upis bajtova
		fileCache.myEntryCache.getEntry ().size = position + size;
		fileCache.myEntryCache.storeOnDisk ();
	}

	while (size > 0) { //od pozicicije do manje od size
		unsigned bufferLoad = ClusterSize - (position % ClusterSize);
		if (bufferLoad > size) { bufferLoad = size; }
		std::memcpy (fileCache.fileData + (position%ClusterSize), buffer + Counter, bufferLoad);
		position += bufferLoad;
		Counter += bufferLoad;
		size -= bufferLoad;
		fileCache.saveClusterDisk ();
		if (position % ClusterSize == 0) {
			if (fileCache.getClusterDisk (position / ClusterSize) == false) {// ako moze u postojeci uzme ako ne dodaje novi klaster za fajl
				fileCache.addClusterDisk ();
			}
		}
	}
	return size == 0;
}

BytesCnt KernelFile::read (BytesCnt size, char * buffer) {
	BytesCnt readCount = 0;
	if (size > fileCache.myEntryCache.getEntry ().size - position) {//ako je size veci od moguceg broja bajtova za ucitavanje uvitaj manji broj
		size = fileCache.myEntryCache.getEntry ().size - position;
	}
	while (readCount < size) {
		unsigned bufferLoad = ClusterSize - (position % ClusterSize);
		if (bufferLoad > size - readCount) { bufferLoad = size - readCount; }
		std::memcpy (buffer + readCount, fileCache.fileData + (position % ClusterSize), bufferLoad);
		position += bufferLoad;
		readCount += bufferLoad;
		if (readCount < size || (position % ClusterSize) == 0) {//kad dodje do kraja klastera,ucitaj sledeci
			fileCache.getClusterDisk (position / ClusterSize);
		}
	}
	return size;
}

char KernelFile::seek (BytesCnt pos) {
	if (pos > fileCache.myEntryCache.getEntry ().size) { return 0; }
	fileCache.getClusterDisk (pos / ClusterSize);
	position = pos;
	return 1;
}

BytesCnt KernelFile::filePos () {
	return position;
}

char KernelFile::eof () {
	return position == fileCache.myEntryCache.getEntry ().size;
}

BytesCnt KernelFile::getFileSize () {
	return fileCache.myEntryCache.getEntry ().size;
}

char KernelFile::truncate () {
	if (mode == 'r') { return 0; }
	int tempPos = position;
	int toDelete = (tempPos + ClusterSize-1) / ClusterSize;
	while (fileCache.freeClusterDisk (toDelete++));
	fileCache.myEntryCache.getEntry ().size = position;//update size
	fileCache.saveClusterDisk();//vrati velicinu fajla na disk
	return 1;
}
