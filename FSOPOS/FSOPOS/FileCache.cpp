#include "FileCache.h"
#include "BitVector.h"

bool FSopos::FileCache::getClusterDisk (int num) {

	if (myEntryCache.getEntry().indexCluster == 0) { return false; }
	char firstLevel[ClusterSize],secondLevel[ClusterSize];
	part->readCluster (myEntryCache.getEntry ().indexCluster, firstLevel);
	if (num >= (ClusterSize / 8)) {
		int firstLvlPoint = (num - 256) / 512 + 256, secondLvlPoint = (num - 256) % 512;//racunanje prvog pointera i drugog za dohvatanje klastera
		location = ((ClusterNo*)firstLevel)[firstLvlPoint];//lokacija prvog nivoa klastera
		if (location == 0) { return false; }
		part->readCluster (location, secondLevel);
		location = ((ClusterNo*)secondLevel)[secondLvlPoint];//lokacija drugog nivoa trazenog klastera
		if (location == 0) { return false; }
		part->readCluster (location, fileData);// lodanje klastera u filedata
	} else {// ako nije u drugom drugoj polovini prvog nivoa
		location = ((ClusterNo*)firstLevel)[num];//
		if (location == 0) { return false; }
		part->readCluster (location, fileData);
	}
	return true;
}

void FSopos::FileCache::saveClusterDisk () {

	part->writeCluster (location, fileData);//vracanje na disk sa lokacije sa koje je zadnje ucitao
	part->readCluster(location, fileData);
}

void FSopos::FileCache::addClusterDisk () {
	char clusterBuffer[ClusterSize],zeroSpace[ClusterSize] = {};
	if (myEntryCache.getEntry ().indexCluster == 0) {
		myEntryCache.getEntry ().indexCluster = bitVctr->takeCluster ();
		part->writeCluster (myEntryCache.getEntry ().indexCluster, zeroSpace);
		myEntryCache.storeOnDisk ();
	}
	location = bitVctr->takeCluster ();
	std::memcpy (fileData, zeroSpace, ClusterSize);
	part->readCluster (myEntryCache.getEntry ().indexCluster, clusterBuffer);
	int	i = 0;
	for (; ((ClusterNo*)clusterBuffer)[i] != 0; i++);//prva nula u indexnom klasteru
	if (i > 256) {
		char secondLevelBuffer[ClusterSize];
		part->readCluster (((ClusterNo*)clusterBuffer)[i - 1], secondLevelBuffer);
		for (int j = 0; j < 512; j++) {
			if (((ClusterNo*)secondLevelBuffer)[j] == 0) {
				((ClusterNo*)secondLevelBuffer)[j] = location;//zapisuje u drugi level lokaciju novog klastera
				part->writeCluster (((ClusterNo*)clusterBuffer)[i - 1], secondLevelBuffer);//update-tuje dodani klaster
				bitVctr->update ();//update-uje bit vektor
				return;
			}
		}
	}
	if (i >= 256) {// ako je zauzet drugi nivo odnosno second level od 512 zauzet, zauzima se novi indexni u second levelu
		*(ClusterNo*)zeroSpace = location;
		((ClusterNo*)clusterBuffer)[i] = bitVctr->takeCluster ();
		part->writeCluster (((ClusterNo*)clusterBuffer)[i], zeroSpace);//ubacuje u drugi nivo na nultu lokaciju lokaciju klastera dodanog
		part->writeCluster (myEntryCache.getEntry ().indexCluster, clusterBuffer);//update-tuje prvi nivo
		bitVctr->update ();
		return;
	}
	((ClusterNo*)clusterBuffer)[i] = location;
	part->writeCluster (myEntryCache.getEntry ().indexCluster, clusterBuffer);
	bitVctr->update ();
	return;
}

bool FSopos::FileCache::freeClusterDisk (int num) {
	char firstLevel[ClusterSize], secondLevel[ClusterSize];
	part->readCluster (myEntryCache.getEntry ().indexCluster, firstLevel);
	if (num >= (ClusterSize / 8)) {
		int firstLvlPointer = (num - 256) / 512 + 256,secondLvlPointer = (num - 256) % 512;
		location = ((ClusterNo*)firstLevel)[firstLvlPointer];
		if (location == 0) { return false; }
		part->readCluster (location, secondLevel);
		location = ((ClusterNo*)secondLevel)[secondLvlPointer];
		if (location == 0) { return false; }
		((ClusterNo*)secondLevel)[secondLvlPointer] = 0;
		part->writeCluster (((ClusterNo*)firstLevel)[firstLvlPointer], secondLevel);
		bitVctr->free (location);
		for (int i = 0; i < ClusterSize / 4; i++) {//provjera ako je citav drugi popunjen nulama, oslobadnja se u prvom indeksnom nivou index
			if (((ClusterNo*)secondLevel)[i] != 0) { return true; }
		}
		bitVctr->free (((ClusterNo*)firstLevel)[firstLvlPointer]);
		((ClusterNo*)firstLevel)[firstLvlPointer] = 0;
		part->writeCluster (myEntryCache.getEntry ().indexCluster, firstLevel);//prvi nivo update
	} else {
		location = ((ClusterNo*)firstLevel)[num];//nadji ako je manji od 256 i postavi na 0
		if (location == 0 || location>(ClusterSize*8) ) { return false; }
		((ClusterNo*)firstLevel)[num] = 0;
		part->writeCluster (myEntryCache.getEntry ().indexCluster, firstLevel);
		bitVctr->free(location);
	}
	bitVctr->update ();//updateuje bitvektor
	return true;
}
