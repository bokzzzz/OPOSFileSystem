#pragma once
#include "EntryCache.h"

namespace FSopos {

	class FileCache {

		FileCache () {};
		bool getClusterDisk (int num); //ucitava cluster po broju sa diska
		void saveClusterDisk (); // snima cluster na disk
		void addClusterDisk (); //dodaje novi klaster sa sadrzajem na disk za fajl
		bool freeClusterDisk (int num); // brise klaster sa diska-sadrzaj fajla

		EntryCache myEntryCache; //entri fajla kesiran
		char fileData[ClusterSize]; // kesirani jedan klaster
		ClusterNo location; //fizicka lokacija na disku
		BitVector* bitVctr;// kesiran bit vektor
		Partition* part; // paricija na kojoj se nalazi fajl

		friend class KernelFile;
		friend class KernelFS;

	};

}
