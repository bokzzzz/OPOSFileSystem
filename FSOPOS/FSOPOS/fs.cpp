#include "fs.h"
#include "KernelFS.h"
#include <Windows.h>

CRITICAL_SECTION FSCSection;

FS::~FS () {
	EnterCriticalSection (&FSCSection);
	delete myImpl;
	LeaveCriticalSection (&FSCSection);
}

char FS::mount (Partition * partition) {
	EnterCriticalSection (&FSCSection);
	auto ret = myImpl->mount (partition);
	LeaveCriticalSection (&FSCSection);
	return ret;
}

char FS::unmount (char part) {
	EnterCriticalSection (&FSCSection);
	auto ret = myImpl->unmount (part);
	LeaveCriticalSection (&FSCSection);
	return ret;
}

char FS::format (char part) {
	EnterCriticalSection (&FSCSection);
	auto ret = myImpl->format (part);
	LeaveCriticalSection (&FSCSection);
	return ret;
}

char FS::readRootDir (char part, EntryNum n, Directory & d) {
	EnterCriticalSection (&FSCSection);
	auto ret = myImpl->readRootDir (part, n, d);
	LeaveCriticalSection (&FSCSection);
	return ret;
}

char FS::doesExist (char * fname) {
	EnterCriticalSection (&FSCSection);
	auto ret = myImpl->doesExist (fname);
	LeaveCriticalSection (&FSCSection);
	return ret;
}

File * FS::open (char * fname, char mode) {
	if (myImpl->openedFiles[*fname - 'A'].find (fname) == myImpl->openedFiles[*fname - 'A'].end ()) {
		InitializeSRWLock (&myImpl->openedFiles[*fname - 'A'][fname]);
	}
	if (mode == 'r') {
		AcquireSRWLockShared (&myImpl->openedFiles[*fname - 'A'][fname]);
	} else { 
		AcquireSRWLockExclusive (&myImpl->openedFiles[*fname - 'A'][fname]);// 'w' ili 'a'
	}
	EnterCriticalSection (&FSCSection);
	auto ret = myImpl->open (fname, mode);
	LeaveCriticalSection (&FSCSection);
	return ret;

}

char FS::deleteFile (char * fname) {
	EnterCriticalSection (&FSCSection);
	auto ret = myImpl->deleteFile (fname);
	LeaveCriticalSection (&FSCSection);
	return ret;
}

FS::FS () {
}

KernelFS* FS::myImpl = new KernelFS ();
