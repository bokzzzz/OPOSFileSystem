#define _CRT_SECURE_NO_WARNINGS
#include "EntryCache.h"
#include <Windows.h>

CRITICAL_SECTION ECCSection;

bool FSopos::EntryCache::loadEntryNumber(int num) {

	EnterCriticalSection(&ECCSection);
	int logicalOffset = 0;
	myDirCluster.getClusterDisk();
	for (int k = 0; k < 102; k++) {
		if (myDirCluster.entries[k].name[0] != 0) {
			if (logicalOffset == num) {
				physicalOffset = k;
				LeaveCriticalSection(&ECCSection);
				return true;
			}
			logicalOffset++;

		}
	}
	LeaveCriticalSection(&ECCSection);
	return false;
}

bool FSopos::EntryCache::loadEntryName(std::string name) {

	EnterCriticalSection(&ECCSection);
	physicalOffset = 0;
	myDirCluster.getClusterDisk();
	for (int k = 0; k < 102; k++) {
		if (myDirCluster.entries[k].name == name) {
			physicalOffset = k;
			LeaveCriticalSection(&ECCSection);
			return true;
		}
	}
	LeaveCriticalSection(&ECCSection);
	return false;

}

void FSopos::EntryCache::storeOnDisk() {

	EnterCriticalSection(&ECCSection);
	Entry tmpE = getEntry();
	myDirCluster.part->readCluster(1, (char *)myDirCluster.entries);
	getEntry() = tmpE;
	myDirCluster.saveClusterDisk();
	LeaveCriticalSection(&ECCSection);

}
//kod kreiranja fajla ime se kopira a indeks i velicina je 0
void resetEntry(Entry& entry, std::string name) {

	std::strcpy(entry.name, name.c_str());
	entry.indexCluster = 0;
	entry.size = 0;
}

void FSopos::EntryCache::createEntry(std::string name) {

	EnterCriticalSection(&ECCSection);
	myDirCluster.getClusterDisk();
	for (int k = 0; k < 102; k++) {
		if (myDirCluster.entries[k].name[0] == 0)
		{
			physicalOffset = k;
			resetEntry(myDirCluster.entries[k], name);
			LeaveCriticalSection(&ECCSection);
			storeOnDisk();
			return;
		}
	}
}
void FSopos::EntryCache::removeEntry() {

	EnterCriticalSection(&ECCSection);
	for (char &x : myDirCluster.entries[physicalOffset].name) { x = 0; }
	for (char &x : myDirCluster.entries[physicalOffset].ext) { x = 0; }
	LeaveCriticalSection(&ECCSection);
	storeOnDisk();
}

Entry & FSopos::EntryCache::getEntry() {

	return myDirCluster.entries[physicalOffset];
}
