#pragma once
#include "FileCache.h"
#include <string>

class File;

class KernelFile {

	KernelFile () {};
	~KernelFile ();
	char write (BytesCnt, char* buffer);
	BytesCnt read (BytesCnt, char* buffer);
	char seek (BytesCnt);
	BytesCnt filePos ();
	char eof ();
	BytesCnt getFileSize ();
	char truncate ();

	std::string absolutePath;//sluzi za vracanje kljuca kod swrLocka
	KernelFS* myFS;
	long position;
	char mode;

	FSopos::FileCache fileCache;//kes jednog klastera fajla

	friend class File;
	friend class KernelFS;

};
