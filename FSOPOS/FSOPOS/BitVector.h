#pragma once
#include "part.h"

class KernelFS;
class KernelFile;

namespace FSopos {

	class FileCache;
	class DirectoryCache;
	class EntryCache;

	class BitVector {
		BitVector (Partition* partition);
		~BitVector ();

		ClusterNo takeCluster (); //
		void free (ClusterNo cluster); // oslobodi bit vektor
		void update (); // Update-tuje bit vektor
		void initialize (); // setuje u kesu sve 0 i uzmima jedan klaster za bitvektor za datu particiju

		Partition* myPartition;
		unsigned char cache[ClusterSize];

		friend class DirectoryCache;
		friend class KernelFS;
		friend class FileCache;

	};

}
